import HomePage from "./Home/HomePage";
import AboutPage from "./About/AboutPage";


export default {
    HomePage: HomePage,
    AboutPage: AboutPage
}