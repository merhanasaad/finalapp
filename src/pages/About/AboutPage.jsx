import React, { Component } from 'react';
import './AboutPage.scss';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronLeft, faChevronRight, faPencilRuler, faQuestionCircle, faImages, faArrowRight } from "@fortawesome/free-solid-svg-icons";


class TechAboutPage extends Component {

    render() {
        return (
            <section className="about-page" to='About'>
                <div className="container">
                    <section className="php">
                        <div className="container">
                            <h3 className="pt-5">All About PHP You Must know</h3>
                            <p className=" pt-4 par">It is a long established fact that a reader will be
                            distracted by the readable content of a page when looking at its layout
                            </p>
                        </div>
                    </section>
                    <section className="all">
                        <div className="container">
                            <div className="row row1 justify-content-center">
                                <div className="col card card1">
                                    <h5 className="pt-4">Environment</h5>
                                    <p>Install Your Environment</p>
                                </div>
                                <div className="col card">
                                    <h5 className="pt-4">Videos</h5>
                                    <p>Technology Related Videos</p>
                                </div>
                                <div className="col card">
                                    <h5 className="pt-4">Overview</h5>
                                    <p>Language Oviewview</p>
                                </div>
                                <div className="col card">
                                    <h5 className="pt-4">Overview</h5>
                                    <p>It is a long established fact that a reader will be distracted.</p>
                                </div>
                            </div>
                        </div>
                    </section>
                    <section className=" why" >
                        <div className="container">
                            <div className="row justify-content-center">
                                <div className="col-7 text-left">
                                    <h2>Why Learn PHP In 2020!!</h2>
                                    <p>Supercharge your agent productivity and improve customer satisfaction
                                    by an average of 45% with our platform. We believe a company is only as good as the people behind it. We go
                                    on workation once a year why we give interns and employees responsibility from day one.
                                    </p>
                                </div>
                                <div className="col-3">
                                    <div className="d-flex info">
                                        <FontAwesomeIcon icon={faPencilRuler} className="icon mr-4" />
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                    </div>
                                    <div className="d-flex info">
                                        <FontAwesomeIcon icon={faQuestionCircle} className="icon mr-4" />
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                    </div>
                                    <div className="d-flex info">
                                        <FontAwesomeIcon icon={faImages} className="icon mr-4" />
                                        <p>Lorem Ipsum is simply dummy text of the printing and typesetting industry</p>
                                    </div>
                                </div >
                            </div>
                        </div>
                    </section>
                    <div className="row justify-content-center lesson-row">
                        <div className="col-6 d-flex justify-content-center">
                            <div className="lesson-img">
                                <img src="" alt="" className="w-100 h-100" />
                            </div>
                            <div className="lesson-tittle">
                                lesson tittle
                            </div>
                        </div>
                        <div className="col-6 d-flex justify-item-center">
                            <div className="lesson-img">
                                <img src="" alt="" className="w-100 h-100" />
                            </div>
                            <div className="lesson-tittle">
                                lesson tittle
                            </div>
                        </div>
                    </div>

                    <div className="row justify-content-center lesson-row">
                        <div className="col-6 d-flex justify-content-center">
                            <div className="lesson-img">
                                <img src="" alt="" className="w-100 h-100" />
                            </div>
                            <div className="lesson-tittle">
                                lesson tittle
                            </div>
                        </div>
                        <div className="col-6 d-flex justify-item-center">
                            <div className="lesson-img">
                                <img src="" alt="" className="w-100 h-100" />
                            </div>
                            <div className="lesson-tittle">
                                lesson tittle
                            </div>
                        </div>
                    </div>
                    <div className="row justify-content-center lesson-row">
                        <div className="col-6 d-flex justify-content-center">
                            <div className="lesson-img">
                                <img src="" alt="" className="w-100 h-100" />
                            </div>
                            <div className="lesson-tittle">
                                lesson tittle
                            </div>
                        </div>
                        <div className="col-6 d-flex justify-item-center">
                            <div className="lesson-img">
                                <img src="" alt="" className="w-100 h-100" />
                            </div>
                            <div className="lesson-tittle">
                                lesson tittle
                            </div>
                        </div>
                    </div>

                    <div className="test-btn-container w-100 text-center">
                        <button className="btn test-btn btn-warning">Test Your PHP Skills</button>
                    </div>

                    <div className="related-tech d-flex align-items-center">
                        <FontAwesomeIcon icon={faArrowRight} className="icon" />
                        <div className="related-tech-img">
                            <img src="" alt="" className="w-100 h-100" />
                        </div>
                        <div className="related-tech-content">
                            <h3>Related Tech</h3>
                            <h2>Laravel</h2>
                            <p>Supercharge your agent productivity and improve customer satisfaction by an average of </p>
                        </div>
                    </div>

                    <div className="swap d-flex justify-content-between">
                        <FontAwesomeIcon icon={faChevronLeft} className="icon" />
                        <ul className="d-flex justify-content-between">
                            <li>Aleana</li>
                            <li>Chavy</li>
                            <li>Fortune</li>
                            <li>Fortune</li>
                        </ul>
                        <FontAwesomeIcon icon={faChevronRight} className="icon" />
                    </div>
                </div>
            </section>
        );
    }

}

export default TechAboutPage;