import React, {Component} from 'react';
import Router from "./Router";
import {Link, NavLink} from "react-router-dom";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faCommentDots} from "@fortawesome/free-solid-svg-icons";
import Header from "./components/Header";

class App extends Component {

    render() {
        return (
            <React.Fragment>
                <Header />
                <div className="container">
                    <Router/>
                </div>
            </React.Fragment>
        );
    }

}

export default App;
