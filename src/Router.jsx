import React from 'react';
import Pages from "./pages";
import {Route, Switch} from "react-router-dom";

const Router = () => (
    <Switch>
        <Route path='/' component={Pages.HomePage} exact/>
        <Route path='/About' component={Pages.AboutPage} exact/>
    </Switch>
);

export default Router;